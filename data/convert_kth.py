import os
import glob
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('--imageSize', default=128, type=int, help='size of image')
parser.add_argument('--dataRoot', default='/path/to/data/', help='data root directory')
opt = parser.parse_args()

image_size = opt.imageSize 
data_root = opt.dataRoot
if not os.path.isdir(data_root):
    raise ValueError('Error with data directory: {}'.format(data_root))

classes = {'boxing', 'handclapping', 'handwaving', 'jogging', 'running', 'walking'}
frame_rate = 25

for _class in classes:
    print(' ---- ')
    print(_class)

    paths = glob.glob(data_root+'/raw/'+_class +'/*')
    for vid in paths:
        print(vid)
        fname = re.split('/', vid)[-1][1-1: -12+1]
        os.system('mkdir -p {}/processed/{}/{}'.format(data_root, _class, fname))
        os.system('ffmpeg -i {}/raw/{}/{} -r {} -f image2 -s {}x{}  {}/processed/{}/{}/image-%03d_{}x{}.png'.format(data_root, _class, re.split('/', vid)[-1], frame_rate, image_size, image_size, data_root, _class, fname, image_size, image_size))

 
